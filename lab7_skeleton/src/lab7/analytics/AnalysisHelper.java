/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab7.analytics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import lab7.entities.Comment;
import lab7.entities.Post;
import lab7.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {
    
   
    
    
    
    // find 5 comments which have the most likes
    // TODO
    public void getFiveLikedComments(){
    Map<Integer,Comment> comments = DataStore.getInstance().getComments();
    Map<Integer, User> users = DataStore.getInstance().getUsers();
    
    
    /*for(User user:users.values())
    {
        for(Comment c : comments.values())
        {
            
        }
    }*/
    List<Comment> commentList = new ArrayList<> (comments.values());
    
    
    Collections.sort(commentList, new Comparator<Comment>(){
        public int compare(Comment c1,Comment c2){
        return Double.compare(c2.getLikes(), c1.getLikes());
        }    
    });
    
    for(int i=0; i<comments.size()&& i<5;i++)
    {
         //System.out.println("test");
        System.out.println(commentList.get(i));
    }
    
    }
	
	
	//Q3
    // find the post with most comments
    public void getPostWithMostComments(){
        Map<Integer,Post> posts = DataStore.getInstance().getPosts();
        List<Post> postList = new ArrayList<> (posts.values());
        List<Integer> maxPostIdList = new ArrayList<Integer>();
        int maxCommentPostId = 0;
        int currentMaxComment = 0;
        int maxComment = 0;
        
        for(int i=0; i<postList.size();i++)
    {
        // get comemnt size of each post object
        currentMaxComment = postList.get(i).getComments().size();
        
        if(currentMaxComment >= maxComment)
        {
            maxComment = currentMaxComment;
            maxCommentPostId = postList.get(i).getPostId();
        }
    }
        // find all the posts having same max comment count
        for(int i=0; i<postList.size();i++)
    {

        if(maxComment == postList.get(i).getComments().size())
        {
            maxPostIdList.add(postList.get(i).getPostId());
        }
    }
        
        System.out.println("Post id " +maxPostIdList + " has most comments");
    }

    //Q1
     public void averageNumberoflikespercomment()
{
   Map<Integer,Comment> comments = DataStore.getInstance().getComments();  //getting the comments
   List<Comment> commentList = new ArrayList<>(comments.values());  // storing it in arraylist 
   
   Iterator itr = commentList.iterator(); // iterating through each object 
   int average = 0 ;
   int c = 0;
   while(itr.hasNext())
   {
       Comment cmnt = (Comment)itr.next();
       average =  average + cmnt.getLikes();
       c = c+1;
       
   }
   int answeraverage = average/c;
   System.out.print("the average Number of likes percoment : " + answeraverage + "");
   

}
    
    //Q2 added by Ashita
    // find user with Most Liked comments
     public void getPostWithMostLikedComments(){
       
        Map<Integer,Comment> comments = DataStore.getInstance().getComments();
         List<Comment> commentList = new ArrayList<>(comments.values());
         
        Collections.sort(commentList, new Comparator<Comment>() {
               public int compare(Comment c1, Comment c2) {
                   return Double.compare(c2.getLikes(), c1.getLikes());
               } 
        });
        Comment maxLiked= commentList.get(0);
            System.out.println("   ");
            System.out.println("Post id " +maxLiked.getPostId() + " has most liked comments");
 
     }

//Q4

public void inactiveBasedOnPosts()
            
    {
        Map<Integer,Integer> inactivePosts = new HashMap<>();
        Map<Integer,Post> posts = DataStore.getInstance().getPosts();
         
        int count = 0;
        for(Post post : posts.values()){
          int postId = 0;
          if(inactivePosts.containsKey(post.getUserId())){
              postId = inactivePosts.get(post.getUserId());
              count = count+1;
          }
          postId += post.getPostId();
          inactivePosts.put(post.getUserId(),count);
         
        }
         System.out.println();
               System.out.println(inactivePosts); 
    
      
  Set<Entry<Integer,Integer>> set = inactivePosts.entrySet();
  List<Entry<Integer,Integer>> list = new ArrayList<Entry<Integer,Integer>>(set);
  Collections.sort(list,new Comparator<Entry<Integer, Integer>>() {
            @Override
            public int compare(Entry<Integer, Integer> o1, Entry<Integer, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }

        }
  
  );

    for(int i = 0;i<list.size() && i<5 ;i++){
        System.out.println("The user with less posts "+list.get(i));
    }
    
    }  

	 //Q7 added by Ashita
     public void getProactiveUserOverall() {
        int noOfLikesOnComment = 0;
            Map<Integer, User> users = DataStore.getInstance().getUsers();
            List<User> userList = new ArrayList<>(users.values());
            Map<Integer, Comment> comments = DataStore.getInstance().getComments();
            List<Comment> commentList = new ArrayList<>(comments.values());

        int noOfPosts = 0;
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        List<Post> postList = new ArrayList<>(posts.values());

        for (User user : userList) {
            noOfPosts = 0;
            for (Post post : postList) {
                if (post.getUserId() == user.getId()) {
                    noOfPosts += 1;
                }
                //userToPost.put(userid, noOfPosts);
                user.setUserPosts(noOfPosts);
            }
        }

        for (User user : userList) {
            noOfLikesOnComment = 0;
            for (Comment comment : commentList) {
                if (comment.getUserId() == user.getId()) {
                    noOfLikesOnComment += comment.getLikes();
                }
                user.setLikesOnComment(noOfLikesOnComment);
            }
        }

        Collections.sort(userList, new Comparator<User>() {
            public int compare(User u1, User u2) {

                return Double.compare(u2.getLikesOnComment()+ u2.getComments().size() + u2.getUserPosts(), u1.getLikesOnComment()+ u1.getComments().size() + u1.getUserPosts());
            }
        });

        System.out.println("Proactive users overall (sum of comments, posts and likes)");
        for (int i = 0; i < users.size() && i < 5; i++) {
            System.out.println(userList.get(i));
        }
     }
       
        //Q6
          public void getinactiveUseroverall() {
        int noOfLikesOnComment = 0;
            Map<Integer, User> users = DataStore.getInstance().getUsers();
            List<User> userList = new ArrayList<>(users.values());
            Map<Integer, Comment> comments = DataStore.getInstance().getComments();
            List<Comment> commentList = new ArrayList<>(comments.values());

        int noOfPosts = 0;
        Map<Integer, Post> posts = DataStore.getInstance().getPosts();
        List<Post> postList = new ArrayList<>(posts.values());

        for (User user : userList) {
            noOfPosts = 0;
            for (Post post : postList) {
                if (post.getUserId() == user.getId()) {
                    noOfPosts += 1;
                }
                //userToPost.put(userid, noOfPosts);
                user.setUserPosts(noOfPosts);
            }
        }

        for (User user : userList) {
            noOfLikesOnComment = 0;
            for (Comment comment : commentList) {
                if (comment.getUserId() == user.getId()) {
                    noOfLikesOnComment += comment.getLikes();
                }
                user.setLikesOnComment(noOfLikesOnComment);
            }
        }

        Collections.sort(userList, new Comparator<User>() {
            public int compare(User u1, User u2) {

                return Double.compare(u2.getLikesOnComment()+ u2.getComments().size() + u2.getUserPosts(), u1.getLikesOnComment()+ u1.getComments().size() + u1.getUserPosts());
            }
        });

        System.out.println("inactive users overall (sum of comments, posts and likes)");
        
        
        for(int i=0 ; i<5 ; i++)
        {
             System.out.println(userList.get(userList.size()-(i+1)));
        }
        
        
        
    }
     
    
	//Q5


public void getInactiveUsersOnComments(){
    
    
            Map<Integer, User> users = DataStore.getInstance().getUsers();
            List<User> userList = new ArrayList<>(users.values());
            Map<Integer, Comment> comments = DataStore.getInstance().getComments();
            List<Comment> commentList = new ArrayList<>(comments.values());
            Map<Integer,Post> posts = DataStore.getInstance().getPosts();
            List<Post> postList = new ArrayList<>(posts.values());
            
            for(User user : userList){
                for(Comment comment : commentList){
                    int noOfComments = 0;
                    if(user.getId() == comment.getUserId()){
                        noOfComments += 1;
                    }
                    user.setLikesOnComment(noOfComments);
                }
            }
            //System.out.println("The user is "+userList);
            Collections.sort(userList, new Comparator<User>(){
                @Override
                public int compare(User t, User t1) {
                    return Double.compare(t.getComments().size(), t1.getComments().size());
                }
                
                
            });
            System.out.println("Inactive Users based on comments");
        for (int i = 0; i < users.size() && i < 5; i++) {
            System.out.println(userList.get(i));
        }     
           
        
}

 
     
}


